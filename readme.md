# VR Prototype

This is an VR Shooting Game Prototype with Unity 2017.4

## Installation

Clone the repository and open in Unity

## Usage

Just Click "Play" button in Unity.
In the game screen press "alt" key on your keyboard and use mouse to turn around.

Target the buttons or enemies to interact. 

## Note about shaders

I didn't write any shader in the game.

### Kaan Tosunoglu
kaan.tosunoglu@gmail.com