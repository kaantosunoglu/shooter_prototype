﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    private float speed;

    public float Speed {
        set {
            speed = value;
            isMoving = true;
        }
    }

    private bool isMoving = false;
    private Vector3 ownPosition;
    private Vector3 target;
    // Use this for initialization
    void Start () {
        ownPosition = transform.position;
        target = new Vector3(0f, ownPosition.y, 0f);
	}
	
	// Enemy movement
	void Update () {
        if (isMoving) {
            float step = speed * Time.deltaTime; // calculate distance to move
            transform.position = Vector3.MoveTowards(transform.position, target, step);
        }
    }

    private void OnTriggerEnter(Collider obj) {
        // if enemy hit to player, player lose health
        if (obj.gameObject.tag == "Player") {
            GameController.instance.PlayerGetHit();
            isMoving = false;
            gameObject.SetActive(false);
        }
    }

    // if player shot the enemy
    public void Hit() {
        isMoving = false;
        GameController.instance.EnemyHit();
        gameObject.SetActive(false);
    }
}
