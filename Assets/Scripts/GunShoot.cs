﻿
using UnityEngine;

public class GunShoot : MonoBehaviour {

    public float range = 100f;
    public Camera cam;


	void Update () {
    // Dedect if user touch the screen
        if (GvrPointerInputModule.Pointer.TriggerDown) {
            RaycastHit hit;
            if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, range)) {
                Debug.Log(hit.transform.tag);

                // if user is looking through to enemy gameobject 
                if (hit.transform.tag == "Enemy") {
                    hit.transform.GetComponent<Enemy>().Hit();
                }
            }
        }
       
    }

    public void Shoot() {

    }
}
