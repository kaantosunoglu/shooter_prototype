﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {
    public static GameController instance;

    public bool isPlaying;

    private float playerHealth;
    private int playerScore;



    private void Awake() {
        if (instance == null) {
            instance = this;
        }

    }

    public void StartGame() {
        isPlaying = true;
        playerHealth = 100;
        playerScore = 0;
        UIController.instance.StartGame();
        UIController.instance.SetScore(playerScore);
        UIController.instance.SetHealth(playerHealth);
        EnemySpawner.instance.StartGame();
    }

    // if enemy hit the player set player health and check if the player dead
    public void PlayerGetHit() {
        playerHealth -= 10;
        UIController.instance.SetHealth(playerHealth);
        if(playerHealth <= 0) {
            isPlaying = false;
            EnemySpawner.instance.GameOver();
            ObjectPooler.instance.GameOver();
            UIController.instance.GameOver(playerScore);
        }
    }


    // if player shot the enemy set score
    public void EnemyHit() {
        playerScore++;
        UIController.instance.SetScore(playerScore);
    }

}
