﻿using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    public static UIController instance;

    public Text textScore;
    public Image imageHealth;
    public Text endScore;
    public GameObject gameUI;
    public GameObject menuUI;
    public GameObject gameOverUI;


    public void Awake() {
        if (instance == null) {
            instance = this;
        }
    }

    public void Start() {
        gameUI.SetActive(false);
        menuUI.SetActive(true);
        gameOverUI.SetActive(false);
    }

    public void SetScore(int score) {
            textScore.text = score.ToString();
    }

    public void SetHealth(float health) {
        imageHealth.fillAmount = 0.0926f + (((0.911f - 0.0926f) / 100f) * health);
    }

    public void GameOver(int score) {
        gameUI.SetActive(false);
        menuUI.SetActive(true);
        gameOverUI.SetActive(true);
        endScore.text = "Your Score: " + score.ToString();
    }

    public void StartGame() {
        gameUI.SetActive(true);
        menuUI.SetActive(false);
        gameOverUI.SetActive(false);
    }

}
