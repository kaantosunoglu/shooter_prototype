﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    public static EnemySpawner instance;
    public GameObject enemy;

    private float spawnTime;
    private float spawnedEnemy;


    private void Awake() {
        if (instance == null) {
            instance = this;
        }

    }


    public void StartGame() {
        spawnedEnemy = 0;
        spawnTime = 5;
        StartCoroutine(Spawn());
    }

    public void GameOver() {
        StopCoroutine(Spawn());
    }


    // Spawn new enemy and set props
    IEnumerator Spawn() {
        if (GameController.instance.isPlaying) {
            spawnedEnemy++;
            float angle = Random.Range(0, 360);
            float positionx = 50 * Mathf.Cos(angle);
            float positionz = 50 * Mathf.Sin(angle);
            Debug.Log(positionx + "  " + positionz);
            Vector3 enemyPosition = new Vector3(positionx, 1.5f, positionz);
            GameObject enemyClone = ObjectPooler.instance.GetFromPool(enemy);
            enemyClone.transform.position = enemyPosition;
            enemyClone.SetActive(true);
            enemyClone.GetComponent<Enemy>().Speed = 10f + (spawnedEnemy / 15);

            spawnTime = spawnTime > 1 ? spawnTime - .25f : spawnTime;
            yield return new WaitForSeconds(spawnTime);
            StartCoroutine(Spawn());
        } else {
            yield return null;
        }
    }
}
