﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour {

    public static ObjectPooler instance;

    public int poolMaxSize = 20;

    private Dictionary<string, List<GameObject>> pool;
    private void Awake() {
        if(instance == null) {
            instance = this;
        }

    }
    void Start () {
        pool = new Dictionary<string, List<GameObject>>();

    }
	

    // get gameobject from pool
	public GameObject GetFromPool(GameObject obj) {

        // look for if the gameobject pool list created before. If not create one
        if(!pool.ContainsKey(obj.tag)) {
            List<GameObject> pooledObjects = new List<GameObject>();
            pool.Add(obj.tag, pooledObjects);
        }

        // look for any not using gameobject in the pool. if yes give one
        for(int i = 0; i < pool[obj.tag].Count; i++) { 
            if(!pool[obj.tag][i].activeInHierarchy) {
                return pool[obj.tag][i];
            }
        }

        // if there is not any gameobject in the pool and poolsize < poolMaxsize then crate one
        if (pool[obj.tag].Count < poolMaxSize) {
            GameObject newObj = (GameObject)Instantiate(obj);
            pool[obj.tag].Add(newObj);
            return newObj;
        }

        // or return null
        return null;
    }


    // when game over remove all game objects from screen
    public void GameOver() {
        foreach (KeyValuePair<string, List<GameObject>> entry in pool) {
            for(int i = 0; i < entry.Value.Count; i++) {
                entry.Value[i].SetActive(false);
            }
        }
    }
}
